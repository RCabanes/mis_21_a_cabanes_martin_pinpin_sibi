class AddRelationshipsToCashiers < ActiveRecord::Migration[5.1]
  def change
    add_column :cashiers, :name, :string
    add_column :cashiers, :status, :boolean
    add_reference :cashiers, :admin, foreign_key: true
  end
end
