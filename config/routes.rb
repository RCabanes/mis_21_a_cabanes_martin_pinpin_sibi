Rails.application.routes.draw do
  get 'summary/index'

  get 'order_lines/index'

  get 'order_lines/new'

  devise_scope :admin do
    get 'admins/sign_in', to: 'publics#index' #failed sign ins will redirect to main page
    authenticated :admin do
      resources :cashiers
      resources :products
      resources :report
      root 'products#index', as: :authenticated_admin_root
      get 'report/index'
    end
  end
  
  devise_scope :cashier do
    get 'cashiers/sign_in', to: 'publics#index' #failed sign ins will redirect to main page
    # added below
    authenticated :cashier do
      resources :products
      resources :orders
      resources :order_lines
      root 'orders#index'
    end
  end

  devise_for :cashiers
  devise_for :admins
  root 'publics#index'
  get 'publics/catalogue'
  get 'order_lines/index'
  get 'order_lines/new'
  get 'orders/new'
  get 'orders/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
