require 'test_helper'

class OrderLinesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get order_lines_index_url
    assert_response :success
  end

  test "should get new" do
    get order_lines_new_url
    assert_response :success
  end

end
