class Order < ApplicationRecord
  has_many :order_lines, inverse_of: 'order'
  accepts_nested_attributes_for :order_lines, reject_if: :all_blank, allow_destroy: true
  belongs_to :cashier
end
