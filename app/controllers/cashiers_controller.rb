class CashiersController < ApplicationController
  before_action :set_cashier, only: [:edit, :show, :update, :destroy
  ]
  
  def index
    @cashiers = Cashier.all
  end

  def new
    @cashier = current_admin.cashiers.new
  end
  
  def create
   @cashier = current_admin.cashiers.new(cashier_params)
   
    @cashier.status = true
    
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully added."
    else
      render :new
     
    end
  end
  
  def edit
  end

  def show
  end
 
  def destroy
    @cashier = Cashier.find(params[:id])
    if @cashier.status == true
      @cashier.status = false
    else
      @cashier.status = true
    end
    
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    end
  end
  
  #version below turns change status to "delete cashier"
  # def destroy
  #   @cashier = Cashier.find(params[:id])
  #   @cashier.destroy
    
  #   redirect_to cashiers_path
  # end
    
  def update
    @cashier = Cashier.find(params[:id])
    @cashier.destroy
    @cashier = current_admin.cashiers.new(cashier_params)
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    else
      render :edit
    end
  end
  
  private
    def set_cashier
      @cashier = Cashier.find_by(id: params[:id])
      redirect_to cashiers_path, notice: "Cashier not found." if @cashier.nil?
    end
    
    def cashier_params
       params.require(:cashier).permit(:email, :password, :password_confirmation, :name) #-> Tried this
    end
    
end
