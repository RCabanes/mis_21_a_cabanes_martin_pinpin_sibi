class OrdersController < ApplicationController
  before_action :set_order, only: [:edit, :show, :update, :destroy]
  
  def index
      @orders = Order.all
      @order_lines = OrderLine.all
  end
  
  def new
      @order = Order.new
  end
  
  def create
      @order = Order.new(order_params)
      @order.cashier = current_cashier
      @order.total_price = 0
      
      if @order.save
        
        redirect_to orders_path, notice: "Order successfully placed."
      else
        render :new
      end
  end
  
  def destroy
    # what?
      @order = Order.find(params[:id])
      @order.destroy
    
      redirect_to orders_path
  end
  
  def back
      redirect_to :back
  end
  
  private
    def set_order
        @order = Order.find_by(id: params[:id])
        redirect_to orders_path, notice: "Order not found." if @order.nil?
    end
    
    def order_params
      params.require(:order).permit(:cashier_id, :total_cash, :cash, order_line_attributes: [:id, :product_id, :order_id, :quantity, :price, :_destroy])
    end
end
