class OrderLinesController < ApplicationController
  # before_action :set_order_line, only: [:edit, :show, :update, :destroy]
  #:inverse_of => :OrderLine
  
  # I don't think there is an index page for order lines.
  # def index
  #   @order_lines = OrderLine.all
  # end
  
  def new
    @order_line = OrderLine.new
    @order_line.order = current_order
  end
  
  def create
    @order_line = OrderLine.new(order_line_params)
    
  end

end
