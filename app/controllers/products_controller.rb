class ProductsController < ApplicationController
  before_action :set_product, only: [:edit, :show, :update, :destroy]
  
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(product_params)
    @product.status = "Active"
    
    if @product.save
      redirect_to products_path, notice: "Product successfully added."
    else
      render :new
    end
  end
  
  def edit
    
  end

  def show
  end
  
  def update
    @product.update(product_params)
    if @product.save
      redirect_to products_path, notice: "Product successfully updated."
    else
      render :edit
    end
  end
  
  def destroy
    if @product.status == true
      @product.status = false
    else
      @product.status = true
    end
    
    if @product.save
      redirect_to products_path, notice: "Product successfully updated."
    end
  end
  
  private
    def set_product
      @product = Product.find_by(id: params[:id])
      redirect_to products_path, notice: "Product not found." if @product.nil?
    end
    
    def product_params
      params.require(:product).permit!
    end
end
