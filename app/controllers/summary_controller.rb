class SummaryController < ApplicationController
  def index
    @order_line = OrderLine.all
    @orders = Order.all
    @products = Product.all
  end
end
